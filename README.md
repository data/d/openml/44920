# OpenML dataset: synchronous_machine

https://www.openml.org/d/44920

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

Synchronous motors (SMs) are AC motors with constant speed.

Synchronous machine data were obtained in real time from the experimental operating environment.

The task is to estimate the excitation current of SM.

**Attribute Description**

First four features are Synchronous motor parameters.

1. *load_current*
2. *power_factor*
3. *power_factor_error*
4. *changing_of_excitation*
5. *excitation_current* - target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44920) of an [OpenML dataset](https://www.openml.org/d/44920). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44920/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44920/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44920/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

